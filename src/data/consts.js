// Directions the robot can face.
export const NORTH = 'NORTH';
export const SOUTH = 'SOUTH';
export const EAST = 'EAST';
export const WEST = 'WEST';

// Upper limits of the table.
export const X_UP = 4;
export const Y_UP = 4;

// Valid commands this program accepts.
export const PLACE = `PLACE [0-${X_UP}],[0-${Y_UP}],(${NORTH}|${SOUTH}|${EAST}|${WEST})`;
export const MOVE = 'MOVE';
export const LEFT = 'LEFT';
export const RIGHT = 'RIGHT';
export const REPORT = 'REPORT';

/**
 * Error messages
 */
// Error message when the user enters wrong input.
export const INVALID_COMMAND = `Error! Invalid command, please enter one of the following commands:
- '${MOVE}' - Move the robot one square forward.
- '${LEFT}' - Rotate the robot 90 degrees to the left.
- '${RIGHT}' - Rotate the robot 90 degrees to the right.
- '${REPORT}' - Displays the X,Y,F of the robot.
- '${PLACE}' - Places the robot on the board eg: PLACE 2,0,SOUTH`;

// Error message when the robot hasn't been placed properly.
export const FIRST_COMMAND = `Error! The place command must be used first and be within the X upper bound: ${X_UP} and the Y upper bound: ${Y_UP}. A valid command would be: PLACE 2,0,SOUTH`;

// Error message when a move command puts the robot off the table.
export const BOUNDS_ERROR = `The move command will move the robot off the table.
The robot can move only within the bounds of x:0-${X_UP} and y:0-${Y_UP}`;
