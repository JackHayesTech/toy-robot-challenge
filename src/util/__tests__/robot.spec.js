import { Robot } from '../robot';
import { NORTH, SOUTH, EAST, WEST, Y_UP, X_UP } from '../../data/consts.js';

describe('Tests for the robot object.', () => {
  let r;
  const logMock = jest.fn();
  console.log = logMock;

  beforeEach(() => {
    r = new Robot();
    jest.clearAllMocks();
  });

  describe('Tests for the isPlaced class function.', () => {
    it('Should return true when position set.', () => {
      r.position = 'test';
      expect(r.isPlaced()).toEqual(true);
    });

    it('Should return false when no position is set.', () =>
      expect(r.isPlaced()).toEqual(false));
  });

  describe('Tests for the place class function.', () => {
    const x = 1;
    const y = 2;
    const f = NORTH;

    it('Should set the correct position for the object.', () => {
      r.place(x, y, f);
      expect(r.position).toEqual({ x, y, f });
    });
  });

  describe('Tests for the report class function.', () => {
    const x = 1;
    const y = 2;
    const f = NORTH;

    it('Should log the correct position of the robot.', () => {
      r.position = { x, y, f };
      r.report();

      expect(logMock).toHaveBeenCalledWith(`Position ${x}, ${y}\nFacing: ${f}`);
    });
  });

  describe('Tests for the rotate class function.', () => {
    //Tests for turning right.
    const right = [
      [NORTH, EAST, 1],
      [EAST, SOUTH, 1],
      [SOUTH, WEST, 1],
      [WEST, NORTH, 1]
    ];
    //Tests for turning left.
    const left = [
      [NORTH, WEST, -1],
      [WEST, SOUTH, -1],
      [SOUTH, EAST, -1],
      [EAST, NORTH, -1]
    ];

    const tests = [...right, ...left];

    it.each(tests)(
      'Should move to direction %p, from direction %p turing %p',
      (start, expected, direction) => {
        r.position = { f: start };
        r.rotate(direction);
        expect(r.position.f).toEqual(expected);
      }
    );
  });

  describe('Tests for the updatePos class function.', () => {
    describe('Tests to successfully move the robot.', () => {
      // Should move the robot north.
      const t1 = ['y', 1, NORTH, { x: 2, y: 3, f: NORTH }];
      // Should move the robot east.
      const t2 = ['x', 1, EAST, { x: 3, y: 2, f: EAST }];
      // Should move the robot south.
      const t3 = ['y', -1, SOUTH, { x: 2, y: 1, f: SOUTH }];
      // Should move the robot east.
      const t4 = ['x', -1, WEST, { x: 1, y: 2, f: WEST }];

      it.each([t1, t2, t3, t4])(
        'Should move the robot along the %p plane heading on the %p direction.',
        (plane, increment, f, expected) => {
          r.position = { x: 2, y: 2, f };
          r.updatePos(plane, increment, 4);
          expect(r.position).toEqual(expected);
        }
      );
    });

    describe('Tests to fail to move the robot.', () => {
      // Fail at moving out the top
      const t1 = ['y', 1, NORTH];
      const t2 = ['x', 1, EAST];
      const t3 = ['y', -1, SOUTH];
      const t4 = ['x', -1, WEST];

      it.each([t1, t2, t3, t4])(
        'Should move the robot along the %p plane heading on the %p direction.',
        (plane, increment, f) => {
          r.position = { x: 0, y: 0, f };
          r.updatePos(plane, increment, 0);
          expect(logMock).toHaveBeenCalled();
        }
      );
    });
  });

  describe('Tests for the move class function.', () => {
    const mock = jest.fn();
    const r = new Robot();
    r.updatePos = mock;
    const t1 = [NORTH, 'y', 1, Y_UP];
    const t2 = [EAST, 'x', 1, X_UP];
    const t3 = [SOUTH, 'y', -1, Y_UP];
    const t4 = [WEST, 'x', -1, X_UP];

    it.each([t1, t2, t3, t4])(
      'Should call the mock with the correct params.',
      (f, plane, increment, bounds) => {
        r.position = { f };
        r.move();
        expect(mock).toHaveBeenCalledWith(plane, increment, bounds);
      }
    );
  });
});
