import { determineCommand } from '../determine-command.js';
import { MOVE, REPORT, RIGHT, LEFT, NORTH } from '../../data/consts.js';

describe('Tests for the determine command utility function.', () => {
  const mock = jest.fn();

  it('Should call the place method with the appropriate parameters.', () => {
    const x = '1';
    const y = '2';
    const input = `PLACE ${x},${y},${NORTH}`;
    const r = {
      place: mock
    };
    determineCommand(input, r);
    expect(mock).toBeCalledWith(x, y, NORTH);
  });

  it('Should call the move method.', () => {
    const r = {
      move: mock
    };
    determineCommand(MOVE, r);
    expect(mock).toBeCalled();
  });

  it.each([
    [RIGHT, 1],
    [LEFT, -1]
  ])(
    'Should call the rotate method via %p with %p paramter.',
    (command, value) => {
      const r = {
        rotate: mock
      };
      determineCommand(command, r);
      expect(mock).toBeCalledWith(value);
    }
  );

  it('Should call the report method.', () => {
    const r = {
      report: mock
    };
    determineCommand(REPORT, r);
    expect(mock).toBeCalled();
  });
});
