import { MOVE, REPORT, PLACE, RIGHT, LEFT } from '../data/consts.js';

/**
 * From the input given by the user determine what function to call for the robot.
 * @param {string} input - The command the user provided.
 * @param {Robot} r - The robot running the commands.
 */
export const determineCommand = (input, r) => {
  if (input.match(PLACE)) {
    // Extracts the coordinates and direction from the command.
    const coords = input.slice(6).split(',');
    r.place(...coords);
  }

  switch (input) {
    case MOVE:
      r.move();
      break;
    case RIGHT:
      r.rotate(1);
      break;
    case LEFT:
      r.rotate(-1);
      break;
    case REPORT:
      r.report();
      break;
    default:
      break;
  }
};
