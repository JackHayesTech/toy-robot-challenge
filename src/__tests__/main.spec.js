jest.mock('../util/determine-command.js');
jest.mock('../util/robot.js');
jest.mock('../util/validate-input.js');
jest.mock('readline');

import { validateInput } from '../util/validate-input.js';
import { determineCommand } from '../util/determine-command.js';
import { main } from '../main.js';

const asyncIterable = {
  async *[Symbol.asyncIterator]() {
    yield* await ['test'];
  }
};

describe('Tests for the main function.', () => {
  // Stub out console for clean test logs.
  console.log = jest.fn();

  beforeEach(() => jest.clearAllMocks());

  it('Should call validate input function when presented with input.', async () => {
    const valMoc = jest.fn();
    validateInput.mockImplementation(valMoc);
    await main(asyncIterable);
    expect(valMoc).toHaveBeenCalled();
  });

  describe('Test to see if determine command is called or not.', () => {
    // Should not call determine command.
    const t1 = [false, 0];
    // Should call determine command.
    const t2 = [true, 1];

    it.each([t1, t2])(
      'When validate input returns %p determine command should be called %p times.',
      async (val, called) => {
        const detMoc = jest.fn();
        const valMoc = jest.fn(() => val);

        validateInput.mockImplementation(valMoc);
        determineCommand.mockImplementation(detMoc);

        await main(asyncIterable);
        expect(detMoc).toHaveBeenCalledTimes(called);
      }
    );
  });
});
