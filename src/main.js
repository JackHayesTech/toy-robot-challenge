import { Robot } from './util/robot.js';
import { validateInput } from './util/validate-input.js';
import { determineCommand } from './util/determine-command.js';

export const main = async (readline) => {
  const r = new Robot();
  console.log('Application Started, please enter command.');

  // Keeps the application paused while waiting for input from the user.
  for await (const input of readline) {
    // Ensures that the users input is valid against the possible commands.
    if (validateInput(input, r.isPlaced())) {
      // Determines the action to be taken by the robot based on the command given.
      determineCommand(input, r);
    }
  }
};
