import rl from 'readline';
import { main } from './main.js';

// Gets user data from cli
const readline = rl.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Runs the main loop of the application.
main(readline);
